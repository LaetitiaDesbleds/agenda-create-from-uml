public class Adress extends ContactDetail {

    public Adress(String value) {
        this.value = value;
    }

    @Override
    public String validate(String value) {
        if (value == null || value.length() <5 ) {
            System.out.println("L'adresse est vide ou ne correspond pas. ");
            System.out.println("Veuillez saisir l'adresse de votre contact : ");
            String newValue = sc.nextLine();
            setValue(newValue);
            return newValue;
        } else {
            System.out.println("L'adresse est valide : " + value);
            return value;
        }
    }
}
