
public class Email extends ContactDetail {

    public Email(String value) {
        this.value = value;
    }

    @Override
    public String validate(String value) {
        String regex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
        if (value.matches(regex)) {
            System.out.println(value + " est une adresse e-mail valide.");
            return value;
        } else {
            System.out.println(value + " n'est pas une adresse e-mail valide.");
            System.out.println("Veuillez saisir l'email de votre contact : ");
            String newValue = sc.nextLine();
            setValue(newValue);
            return newValue;
        }

    }
}
