import java.io.Serializable;
import java.util.List;

public class Contact implements Serializable {

    public String name;
    private Phone phone;
    private Email email;
    private Adress adresse;
    private Website website;

    public Contact() {
    }

    public Contact(String name, Phone phone, Email email, Adress adresse, Website website) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.adresse = adresse;
        this.website = website;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public Adress getAdresse() {
        return adresse;
    }

    public void setAdresse(Adress adresse) {
        this.adresse = adresse;
    }

    public Website getWebsite() {
        return website;
    }

    public void setWebsite(Website website) {
        this.website = website;
    }

    @Override
    public String toString() {
        return "Contact{" +
                name +
                ", " + phone +
                ", " + email +
                ", " + adresse +
                ", " + website +
                '}';
    }
}
