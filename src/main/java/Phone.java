public class Phone extends ContactDetail {

    public Phone(String value) {
        this.value = value;
    }

    @Override
    public String validate(String value) {
        String regex = "^(?:(?:\\+|00)\\d{1,3}[-\\s]?)?(?:\\d{2,3}[-\\s]?){2,3}\\d{2,3}$";
        if (value.matches(regex)) {
            System.out.println(value + " est un numéro de téléphone valide.");
            return value;
        } else {
            System.out.println(value + " n'est pas un numéro de téléphone valide.");
            System.out.println("Veuillez saisir le numéro de téléphone de votre contact : ");
            String newValue = sc.nextLine();
            setValue(newValue);
            return newValue;
        }

    }

}
