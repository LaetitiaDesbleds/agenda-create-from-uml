import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User {

    public String login;
    private String password;
    private String jwt_token;
    //private Agenda agenda;
    List<Agenda> agendas = new ArrayList<>();

    public User() {
    }

    public User(String login, String password, String jwt_token, List<Agenda> agendas) {
        this.login = login;
        this.password = password;
        this.jwt_token = jwt_token;
        this.agendas = agendas;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public List<Agenda> getAgendas() {
        return agendas;
    }

    public void setAgendas(List<Agenda> agendas) {
        this.agendas = agendas;
    }

    private void addAgenda (Agenda agenda){
        for (int i = 0; i < agendas.size(); i++) {
            if (agendas.get(i) == null) {
                agendas.set(i, agenda);
            }
        }


    }
}
