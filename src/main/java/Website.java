import java.util.Scanner;

public class Website extends ContactDetail {

    private String value;
    public Website(String value) {
        this.value = value;
    }



    @Override
    public String validate(String value) {
        String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        if (value.matches(regex)) {
            System.out.println(value + " est un une url de site web valide.");
            return value;
        } else {
            System.out.println(value + " n'est pas une url valide.");
            System.out.println("Veuillez saisir le website de votre contact : ");
            String newValue = sc.nextLine();
            setValue(newValue);
            return newValue;
        }
    }


}
