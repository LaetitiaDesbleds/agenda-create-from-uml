import java.util.List;

public class Agenda {

    private User users;
    private List<Contact> contacts;

    public Agenda() {
    }

    public Agenda(User users, List<Contact> contacts) {
        this.users = users;
        this.contacts = contacts;
    }

    public Agenda(User user1) {
    }

    public User getUsers() {
        return users;
    }

    public void setUsers(User users) {
        this.users = users;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "Agenda{" +
                ", users=" + users +
                ", contacts=" + contacts +
                '}';
    }

    private Agenda createAgenda(Integer id, User user1){
        return new Agenda(user1);
    }
}
