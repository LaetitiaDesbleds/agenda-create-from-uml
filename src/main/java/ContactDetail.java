import java.io.Serializable;
import java.util.Scanner;

public abstract class ContactDetail implements Serializable {

    public String value;
    private Contact owner;
    Scanner sc = new Scanner(System.in);

    public abstract String validate(String value);

    public String toString() { return value; }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
