import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // Demander à l'utilisateur de saisir les informations pour créer un agenda
        System.out.println("Veuillez saisir votre nom d'utilisateur : ");
        String login = scanner.nextLine();
        System.out.println("Veuillez saisir votre mot de passe : ");
        String password = scanner.nextLine();
        System.out.println("Veuillez saisir un token JWT : ");
        String jwt_token = scanner.nextLine();

        // Créer un nouvel utilisateur avec les informations saisies
        User user = new User(login, password, jwt_token, new ArrayList<>());

        // Demander à l'utilisateur de saisir les informations pour créer un contact
        System.out.println("Veuillez saisir le nom de votre contact : ");
        String contactName = scanner.nextLine();

        System.out.println("Veuillez saisir le numéro de téléphone de votre contact : ");
        String phoneNumber = scanner.nextLine();
        Phone phone1 = new Phone(phoneNumber);
        phone1.validate(phoneNumber);

        System.out.println("Veuillez saisir l'email de votre contact : ");
        String email = scanner.nextLine();
        Email email1 = new Email(email);
        email1.validate(email);

        System.out.println("Veuillez saisir l'adresse de votre contact : ");
        String adresse = scanner.nextLine();
        Adress adress1 = new Adress(adresse);
        adress1.validate(adresse);

        System.out.println("Veuillez saisir le website de votre contact : ");
        String website = scanner.nextLine();
        Website website1 = new Website(website);
        website1.validate(website);


        // Créer un nouveau contact avec les informations saisies
        Contact contact = new Contact(contactName, phone1, email1, adress1, website1);

        // Créer un nouvel agenda avec l'utilisateur et le contact créé
        Agenda agenda = new Agenda(user, Arrays.asList(contact));

        // Ajouter l'agenda créé à la liste d'agendas de l'utilisateur
        user.getAgendas().add(agenda);

        // Afficher les informations de l'agenda créé
        System.out.println("Agenda créé avec succès : ");
        System.out.println("Nom d'utilisateur : " + login);
        System.out.println("Contact ajouté : " + contact);
    }

    }


