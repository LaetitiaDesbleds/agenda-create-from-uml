1) comment installer lancer le projet
Les différentes étapes ci dessous résument l'installation et le lancement du projet java 
    - Installer un jdk (Précision java 18)
    - Avoir téléchargé le code source du projet(en clonant ou forkant le projet à partir de gitlab)
    - Compiler le code source à partir de la commande javac : javac + nomProjet.java
    - Exécuter le projet grâce à la commande java : java + nomProjet

2) Comment lancer un environnement de dev
    - Avoir installé un IDE (eclipse, intellij...)
    - Importer le code source dans votre IDE 
    - S'assurer que toutes les dépendances de votre projet sont bien installées et à jour
    - Exécuter votre code source pour s'assurer de son fonctionnement

Schéma :
![agenda_from_uml.PNG](./img/agenda_from_uml.PNG)
